#!/bin/bash

apt-get update
cd ~

# instllation env arduino
apt-get install curl -y
curl -L -o arduino-cli.tar.bz2 https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux64.tar.bz2
tar xjf arduino-cli.tar.bz2
rm arduino-cli.tar.bz2
mv `ls -1` /usr/bin/arduino-cli
arduino-cli core update-index


# Installation de pytho, pip & pyserial

apt-get install python3 pip -y
pip install pyserial

# Installation esp32 core
printf "board_manager:\n  additional_urls:\n    - https://dl.espressif.com/dl/package_esp32_index.json\n" > .arduino-cli.yaml
arduino-cli core update-index --config-file .arduino-cli.yaml
arduino-cli core install esp32:esp32 --config-file .arduino-cli.yaml

# Installation 'native' packages
arduino-cli lib install "Adafruit BME280 Library"
arduino-cli lib install "Adafruit Unified Sensor"
arduino-cli lib install "HCSR04 ultrasonic sensor"
arduino-cli lib install "ArduinoJson"
arduino-cli lib install "MPU9250_asukiaaa"
arduino-cli lib install "ODriveArduino"
cd -
 
